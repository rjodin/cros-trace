// Copyright 2022 Google LLC
// SPDX-License-Identifier: MIT

#pragma once

#include "tool-common.h"

int
tool_perfetto_main(int argc, char **argv);
