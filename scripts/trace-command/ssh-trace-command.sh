#!/bin/bash

set -xe

SCRIPT_DIR=$(dirname $(realpath "${BASH_SOURCE[0]}"))
DUT="$1"
shift
COMMAND="$@"

scp -C "${SCRIPT_DIR}/trace-command.sh" "${SCRIPT_DIR}/../../configs/perfetto.cfg" "${DUT}":/tmp/
ssh "${DUT}" bash /tmp/trace-command.sh "${COMMAND}"
scp -C "${DUT}":"$(ssh "${DUT}" readlink -f /tmp/trace-command-latest.trace)" .
