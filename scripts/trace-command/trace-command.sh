#!/bin/bash

set -x

SCRIPT_DIR=$(dirname $(realpath "${BASH_SOURCE[0]}"))

OUTPUT_TRACE_NAME="$(basename $(echo "$@" | sed 's|^\([^ ]*\).*$|\1|')).$(date +%y%m%d-%H%M%S).trace"
OUTPUT_TRACE="${SCRIPT_DIR}/${OUTPUT_TRACE_NAME}"
CONFIG_FILE="${SCRIPT_DIR}/perfetto.cfg"

PRODUCER_PID=0
if which pps-producer
then
    pps-producer &
    PRODUCER_PID=$!
elif which gpudataproducer
then
    gpudataproducer &
    PRODUCER_PID=$!
fi

PERFETTO_PID="$(perfetto -c ${CONFIG_FILE} --txt -o "${OUTPUT_TRACE}" --background-wait)"

$@

kill ${PERFETTO_PID}
# Cannot use 'wait' because process is not a child of this shell
while [[ -n $(pgrep -x perfetto) ]]; do sleep 0.2; done

if [[ ${PRODUCER_PID} -ne 0 ]]
then
    kill ${PRODUCER_PID}
fi

ln -fs "${OUTPUT_TRACE_NAME}" ${SCRIPT_DIR}/trace-command-latest.trace
