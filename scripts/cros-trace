#!/usr/bin/env python3
#
# Copyright 2022 Google LLC
# SPDX-License-Identifier: MIT

import argparse
import datetime
from pathlib import Path
import shutil
import subprocess
import sys

VERBOSE = False

class ConState:
    CROS = 0
    ANDROID = 1
    LINUX = 2
    VSH_LXD = 3
    VSH_CONTAINERLESS = 4

    def __init__(self):
        self.system = None
        self.uid = None

        self.work_dir = None
        self.work_trace = None
        self.work_tool = None
        self.work_perfetto = None
        self.work_pid_file = None

        self.config_txt = None

        self.out_trace = None
        self.out_trace_symlink = None

        self.cleanup_files = []
        self.cleanup_cmds = []

    def cleanup(self, con):
        if self.cleanup_files:
            args = ['rm', '-f'] + self.cleanup_files
            con.run(args, check=False)
            self.cleanup_files.clear()

        for cmd in self.cleanup_cmds:
            con.run(cmd, check=False)
        self.cleanup_cmds.clear()

class Con:
    def __init__(self, name, wrapper):
        self.name = name
        self.wrapper = wrapper
        # this is used by Session, not Con
        self.state = ConState()

    def _fix_args(self, args, kwargs):
        wrap = kwargs.pop('wrap', True)

        kwargs.setdefault('stdout', subprocess.PIPE)
        kwargs.setdefault('encoding', 'utf-8')

        if wrap and self.wrapper:
            if isinstance(args[0], list):
                # is this the way to modify args?
                l = list(args)
                l[0] = self.wrapper + l[0]
                args = tuple(l)
            else:
                raise RuntimeError('incomplete run support')

        return args, kwargs

    def popen(self, *args, **kwargs):
        args, kwargs = self._fix_args(args, kwargs)

        if VERBOSE:
            print(f'popening {args[0]}')
        return subprocess.Popen(*args, **kwargs)

    def run(self, *args, **kwargs):
        kwargs.setdefault('check', True)
        args, kwargs = self._fix_args(args, kwargs)

        if VERBOSE:
            print(f'running {args[0]}')
        return subprocess.run(*args, **kwargs)

    def run_script(self, script, **kwargs):
        sh = 'sh' if isinstance(self, ADB) else 'bash'
        return self.run([sh], input=script, **kwargs)

    def which(self, name):
        p = self.run(['which', name], check=False, stderr=subprocess.DEVNULL)
        if p.returncode:
            return None
        else:
            return p.stdout.strip()

    def perfetto_version(self, perfetto):
        p = self.run(['perfetto', '--version'], check=False,
                     stderr=subprocess.DEVNULL)
        if p.returncode:
            return 9
        else:
            ver = p.stdout[10:].split('.')[0]
            return int(ver)

    def start_perfetto(self, tool, pid_file, exec_path, output, config_txt):
        args = [tool, 'perfetto', '--start', '--pid-file', pid_file,
                '--background', '--timesync', '--exec-path', exec_path,
                '--output-path', output, '--txt']

        self.run(args, input=config_txt, encoding=None)

    def kill_perfetto(self, tool, pid_file):
        args = [tool, 'perfetto', '--kill', '--pid-file', pid_file]
        self.run(args)

    def wait_perfetto(self, tool, pid_file):
        args = [tool, 'perfetto', '--wait', '--pid-file', pid_file]
        self.run(args)

    def copy_to(self, local_file, remote_file):
        raise RuntimeError('no copy_to')

    def copy_back(self, remote_file, local_file):
        raise RuntimeError('no copy_back')

    def move_back(self, remote_file, local_file):
        self.copy_back(remote_file, local_file)
        self.run(['rm', remote_file])

class Local(Con):
    def __init__(self):
        super().__init__('local', [])

    def copy_to(self, local_file, remote_file):
        self.run(['cp', local_file, remote_file])

    def copy_back(self, remote_file, local_file):
        self.run(['cp', remote_file, local_file])

    def move_back(self, remote_file, local_file):
        self.run(['mv', remote_file, local_file])

class SSH(Con):
    def __init__(self, remote):
        super().__init__(remote, ['ssh', remote])
        self.remote = remote

    def copy_to(self, local_file, remote_file):
        self.run(['scp', '-C', local_file, f'{self.remote}:{remote_file}'],
                 wrap=False)

    def copy_back(self, remote_file, local_file):
        self.run(['scp', '-C', f'{self.remote}:{remote_file}', local_file],
                 wrap=False)

class ADB(Con):
    def __init__(self, remote=None):
        if remote:
            name = remote
            adb = ['adb', '-s', remote]
        else:
            name = 'adb'
            adb = ['adb']

        super().__init__(name, adb + ['shell'])
        self.adb = adb

    def copy_to(self, local_file, remote_file):
        self.run(self.adb + ['push', '--sync', local_file, remote_file],
                 wrap=False)

    def copy_back(self, remote_file, local_file):
        self.run(self.adb + ['pull', remote_file, local_file], wrap=False)


class VSH(Con):
    def __init__(self, dut: str, remote: str):
        pieces = remote.rsplit(':', 1)
        vm = pieces[-1]
        host = ':'.join(pieces[0:-1])
        if not host:
            host = dut

        c = SSH(host)
        owner_id = c.run_script(
            r'dbus-send \
            --system \
            --dest=org.chromium.SessionManager \
            --print-reply \
            --type=method_call \
            /org/chromium/SessionManager \
            org.chromium.SessionManagerInterface.RetrievePrimarySession \
            | awk "NR==3" \
            | cut -d "\"" -f2').stdout.strip()
        super().__init__(host+':'+vm,
                         ['ssh', host, '--', 'vsh', f'--vm_name={vm}', f'--owner_id={owner_id}', '--'])
        self.remote = remote

    def copy_to(self, local_file, remote_file):
        with open(local_file, 'rb') as f:
            payload = f.read()
            self.run(['cp', '/proc/self/fd/0', remote_file],
                     wrap=True, input=payload, encoding=None)

    def copy_back(self, remote_file, local_file):
        payload = self.run(['cat', remote_file],
                           wrap=True, encoding=None).stdout
        with open(local_file, 'wb') as f:
            f.write(payload)


GET_TSC_OFFSET = r'''
find_tsc_offsets="find /sys/kernel/debug/ -name tsc-offset -or -name cntvoff"

vm_pids=$(${find_tsc_offsets} \
          | sed -r 's/.*kvm\/([0-9]*).*/\1/g' | sort | uniq)

if [[ $(echo "{vm_pids}" | wc -w) -gt "2" ]]; then
  echo "error: more than 2 VMs are running"
  exit 1
fi

found_guest_pid=''
for pid in ${vm_pids}
do
  cmd_arcvm=$(cat /proc/${pid}/cmdline \
              | grep arcvm > /dev/null && echo arcvm)
  if [[ "${is_arcvm}" == true ]]; then
    if [[ "${cmd_arcvm}" == "arcvm" ]]; then
      found_guest_pid="${pid}"
    fi
  else
    if [[ ! "${cmd_arcvm}" == "arcvm" ]]; then
      found_guest_pid="${pid}"
    fi
  fi
done

if [[ -z "${found_guest_pid}" ]] && [[ "${is_arcvm}" == true ]]; then
  # ARC++ is detected.
  tsc_offset=0
else
  if [[ -z "${found_guest_pid}" ]]; then
    echo "error: guest tsc offset not found -- is guest running?"
    exit 1
  fi

  tsc_offset_files=$(${find_tsc_offsets} \
              | grep ${found_guest_pid}-)

  # Make sure the tsc offsets are all the same for each vcpu.
  tsc_offset=$(${find_tsc_offsets} \
              | grep ${found_guest_pid}- | xargs cat | uniq)


  for f in ${tsc_offset_files}; do
    f=`basename $f`
    # The cntvoff in debugfs on arm64 inverts the sign compared to
    # tsc-offset on x86:
    if [ "$f" = "cntvoff" ]; then
      tsc_offset=$((-tsc_offset))
    fi
    break
  done

  if [[ ! $(echo "${tsc_offset}" | wc -w) == "1" ]]; then
    echo "error: guest tsc offsets are inconsistent; file a bug on this script"
    exit 1
  fi
fi

echo "$tsc_offset"
'''

class Session():
    def __init__(self, args):
        self.cons = []
        self.tsc_offset = 0
        self.pps_producer = args.pps_producer

        self._init_input_paths(args)
        self._init_cons(args)
        self._init_output_paths(args)

        con_names = [con.name for con in self.cons]
        print('trace session:')
        print(f'  config: {self.config_path}')
        print(f'  output dir: {self.output_dir}')
        print(f'  connections: {con_names}')
        if self.guest:
            print(f'  bin dir: {self.bin_dir}')
            print(f'  data dir: {self.data_dir}')

    def __del__(self):
        for con in self.cons:
            con.state.cleanup(con)

    def _init_input_paths(self, args):
        if args.bin_dir:
            p = Path(args.bin_dir)
            if not p.is_dir():
                raise RuntimeError(f'{args.bin_dir} is not a directory')
            self.bin_dir = p.resolve()
        else:
            self.bin_dir = Path(__file__).parent.resolve()

        self.data_dir = None
        if args.data_dir:
            p = Path(args.data_dir)
            if not p.is_dir():
                raise RuntimeError(f'{args.data_dir} is not a directory')
            self.data_dir = p.resolve()
        else:
            candidates = [
                self.bin_dir.parent / 'share' / 'cros-trace',
                self.bin_dir,
            ]

            for d in candidates:
                if (d / 'configs').exists():
                    self.data_dir = d.resolve()

        p = Path(args.config)
        if not p.exists() and len(p.parts) == 1 and self.data_dir:
            p = self.data_dir / 'configs' / p
        if not p.exists():
            raise RuntimeError(f'perfetto config {args.config} does not exist')
        self.config_path = p.resolve()

        self.config_duration_ms = args.time * 1000 if args.time else None

        p = self.bin_dir / 'cros-trace-tool'
        if p.exists():
            self.exec_tool = p.resolve()
        else:
            self.exec_tool = self._which('cros-trace-tool')

        self.exec_protoc = self._which('protoc')
        self.exec_ssh = self._which('ssh')
        self.exec_adb = self._which('adb')

    def _init_cons(self, args):
        # determine dut con
        self.dut = None
        if args.dut is not None:
            self.dut = SSH(args.dut)
        elif args.local:
            self.dut = Local()

        # determine guest con
        self.guest = None
        if args.guest is not None:
            self.guest = SSH(args.guest)
        elif args.android is not None:
            self.guest = ADB(args.android)
        elif args.vsh_guest is not None:
            self.guest = VSH(args.dut, args.vsh_guest)

        # when there is only guest, treat it as dut
        if not self.dut:
            self.dut = self.guest
            self.guest = None

        self.cons.append(self.dut)
        if self.guest:
            self.cons.append(self.guest)

        for con in self.cons:
            if isinstance(con, SSH) and not self.exec_ssh:
                raise RuntimeError('failed to find ssh executable')
            elif isinstance(con, ADB) and not self.exec_adb:
                raise RuntimeError('failed to find adb executable')

    def _init_output_paths(self, args):
        self.output_dir = Path(args.output_dir).resolve()

        out_ts = datetime.datetime.now().strftime('%Y%m%d-%H%M%S')
        out_fn = f'{args.output_dir}/cros-trace-{out_ts}'
        out_sym = f'{args.output_dir}/cros-trace-latest'

        if self.guest:
            self.dut.state.out_trace = f'{out_fn}-dut.trace'
            self.dut.state.out_trace_symlink = f'{out_sym}-dut.trace'

            self.guest.state.out_trace = f'{out_fn}-guest.trace'
            self.guest.state.out_trace_symlink = f'{out_sym}-guest.trace'

            self.out_merged_trace = f'{out_fn}-merged.trace'
            self.out_merged_trace_symlink = f'{out_sym}-merged.trace'

            self.out_tsc_offset = f'{out_fn}-tsc-offset'
            self.out_tsc_offset_symlink = f'{out_sym}-tsc-offset'
        else:
            self.dut.state.out_trace = f'{out_fn}.trace'
            self.dut.state.out_trace_symlink = f'{out_sym}.trace'

    def setup(self):
        print('setting up...')

        Path(self.output_dir).mkdir(parents=True, exist_ok=True)

        self._setup_config()

        # setup system/uid
        for con in self.cons:
            self._setup_system(con)

        if self.guest:
            self._setup_tsc_offset()
        if self.pps_producer:
            self._setup_pps_producer()

        for con in self.cons:
            self._setup_work_paths(con)

            if con.state.system == ConState.ANDROID:
                self._setup_android_perfetto(con)

            self._setup_tool(con)
            self._setup_config_txt(con)

    def _setup_config(self):
        with open(self.config_path) as f:
            lines = [line.rstrip() for line in f.readlines()]

        duration_ms = None
        for line in lines:
            if line.startswith('duration_ms'):
                idx = line.index(':') + 1
                duration_ms = int(line[idx:].strip())
                break

        if duration_ms is None and self.config_duration_ms:
             lines.append(f'duration_ms: {self.config_duration_ms}')
        elif duration_ms is not None:
            if (self.config_duration_ms and duration_ms != self.config_duration_ms):
                raise RuntimeError(f'config already set duration_ms to {duration_ms}')
            self.config_duration_ms = duration_ms

        self.config_lines = lines

    def _setup_system(self, con):
        if isinstance(con, ADB):
            sys = ConState.ANDROID
        elif isinstance(con, VSH):
            p = con.run(['test', '-d', '/mnt/stateful/lxd_conf'], check=False)
            sys = ConState.VSH_CONTAINERLESS if p.returncode else ConState.VSH_LXD
        else:
            p = con.run(['test', '-d', '/mnt/stateful_partition'],
                    check=False)
            sys = ConState.LINUX if p.returncode else ConState.CROS

        con.state.system = sys
        con.state.uid = int(con.run(['id', '-u']).stdout.strip())

        sys_name = {
            ConState.CROS: 'Chrome OS',
            ConState.ANDROID: 'Android',
            ConState.LINUX: 'Linux',
            ConState.VSH_LXD: 'Vsh to container',
            ConState.VSH_CONTAINERLESS: 'Vsh without container',
        }[sys]
        print(f'  {con.name}: {sys_name}, uid {con.state.uid}')

    def _setup_pps_producer(self):
        con = self.dut
        producer = ''
        if con.state.uid:
            return
        if con.which('pps-producer'):
            producer = 'pps-producer'
        elif con.which('gpudataproducer'):
            producer = 'gpudataproducer'
        else:
            return

        p = con.run(['pgrep', '-x', producer], check=False)
        if p.returncode:
            con.popen([producer], stdout=subprocess.DEVNULL,
                      stderr=subprocess.DEVNULL)
            con.state.cleanup_cmds.append(['pkill', '-x', producer])
            status = 'started'
        else:
            status = 'is already running'

        print(f'  {con.name}: {producer} {status}')

    def _setup_work_paths(self, con):
        work_dir = self._get_work_dir(con)
        con.run(['mkdir', '-p', work_dir])

        con.state.work_dir = work_dir
        con.state.work_trace = f'{work_dir}/cros-trace-perfetto.trace'
        # these might need fixups later
        con.state.work_tool = None
        con.state.work_perfetto = 'perfetto'
        con.state.work_pid_file = f'{work_dir}/cros-trace-tool-perfetto.pid'

    def start_tracing(self):
        print('starting...')

        for con in self.cons:
            con.start_perfetto(con.state.work_tool, con.state.work_pid_file,
                               con.state.work_perfetto, con.state.work_trace,
                               con.state.config_txt)
            con.state.cleanup_files.append(con.state.work_trace)
            con.state.cleanup_cmds.append([con.state.work_tool, 'perfetto',
                                           '--stop', '--pid-file',
                                           con.state.work_pid_file])
            print(f'  {con.name}: perfetto started')

    def wait_tracing(self):
        if self.config_duration_ms:
            print(f'tracing for {self.config_duration_ms}ms... (Ctrl-C to interrupt)')
        else:
            print('tracing... (Ctrl-C to stop)')

        ctrl_c = 0
        while True:
            try:
                if ctrl_c:
                    print('  waiting for perfetto to terminate...')

                for con in self.cons:
                    con.wait_perfetto(con.state.work_tool,
                                      con.state.work_pid_file)
                    print(f'  {con.name}: perfetto stopped')
                break
            except KeyboardInterrupt:
                if not ctrl_c:
                    for con in self.cons:
                        con.kill_perfetto(con.state.work_tool,
                                          con.state.work_pid_file)

                ctrl_c += 1
                if ctrl_c >= 3:
                    raise

    def collect_traces(self):
        print('collecting traces...')
        for con in self.cons:
            con.move_back(con.state.work_trace, con.state.out_trace)
            con.state.cleanup_files.remove(con.state.work_trace)
            self._symlink(con.state.out_trace, con.state.out_trace_symlink)
            print(f'  collected {con.state.out_trace}')

        if self.guest:
            with open(self.out_tsc_offset, 'w') as f:
                print(self.tsc_offset, file=f)
            self._symlink(self.out_tsc_offset, self.out_tsc_offset_symlink)
            print(f'  saved {self.out_tsc_offset}')

    def cleanup(self):
        print('cleaning up...')

        for con in self.cons:
            con.state.cleanup(con)

    def merge_traces(self):
        if not self.guest:
            return

        print('merging traces...')
        if not self.exec_tool:
            print(f'  could not find {self.bin_dir}/cros-trace-tool')
            return

        Local().run([self.exec_tool,
                     'merge',
                     '--output-trace',
                     self.out_merged_trace,
                     '--dut-trace',
                     self.dut.state.out_trace,
                     '--guest-trace',
                     self.guest.state.out_trace,
                     '--cpu-tick-offset',
                     str(self.tsc_offset)])
        self._symlink(self.out_merged_trace, self.out_merged_trace_symlink)
        print(f'  created {self.out_merged_trace}')

    def _get_work_dir(self, con):
        if con.state.system == ConState.ANDROID:
            # use /data/misc/perfetto-traces?
            return '/data/local/tmp/cros-trace'
        elif con.state.system == ConState.CROS and con.state.uid == 0:
            return '/usr/local/cros-trace'
        elif con.state.system == ConState.VSH_LXD:
            return '/mnt/stateful/lxd_conf/cros-trace'
        elif con.state.system == ConState.VSH_CONTAINERLESS:
            return '/home/chronos/cros-trace'
        else:
            return '/tmp/cros-trace'

    def _which(self, name):
        path = shutil.which(name)
        return Path(path) if path else None

    def _symlink(self, out_src, out_dst):
        tmp = Path(f'{out_src}-symlink')
        tmp.symlink_to(Path(out_src).name)
        tmp.rename(out_dst)

    def _setup_tsc_offset(self):
        con = self.dut
        if con.state.uid != 0:
            return

        is_arcvm = 'true' if self.guest.state.system == ConState.ANDROID else 'false'
        script = f'is_arcvm={is_arcvm}' + GET_TSC_OFFSET

        p = con.run_script(script, check=False)
        if p.returncode:
            print(p.stdout)
        else:
            tsc_offset = 0
            for line in p.stdout.split():
                try:
                    tsc_offset = int(line)
                except ValueError:
                    pass

            print(f'  {con.name}: tsc_offset {tsc_offset}')
            self.tsc_offset = tsc_offset

    def _setup_android_perfetto(self, con):
        # prefer tracebox sideloaded by record_android_trace
        path_perfetto = '/data/local/tmp/tracebox'
        p = con.run(['test', '-x', path_perfetto], check=False)
        if p.returncode:
            path_perfetto = con.run(['which', 'perfetto']).stdout.strip()

        # copy perfetto to work around selinux rules
        work_perfetto = f'{con.state.work_dir}/cros-trace-perfetto'
        con.run(['cp', path_perfetto, work_perfetto])
        con.state.cleanup_files.append(work_perfetto)
        con.state.work_perfetto = work_perfetto

    def _setup_tool(self, con):
        con.state.work_tool = con.which('cros-trace-tool')
        if not con.state.work_tool and self.data_dir:
            mach = con.run(['uname', '-m']).stdout.strip()
            if mach == 'aarch64' and con.state.system == ConState.CROS:
                p = con.run(['test', '-d', '/lib64'], check=False)
                if p.returncode:
                    mach = 'armv7a'
            sys = 'android' if con.state.system == ConState.ANDROID else 'linux'

            p = Path(self.data_dir / f'{sys}-{mach}' / 'cros-trace-tool')
            if p.exists():
                work_tool = f'{con.state.work_dir}/cros-trace-tool'
                con.copy_to(str(p), work_tool)
                con.run(['chmod', 'u+x', work_tool])
                con.state.work_tool = work_tool

        if not con.state.work_tool:
            raise RuntimeError('failed to find cros-trace-tool')

        print(f'  {con.name}: tool at {con.state.work_tool}')

    def _setup_config_txt(self, con):
        optional_params = {
            'cpufreq_period_ms': 24,
            'disable_incremental_timestamps': 25,
        }

        ver = con.perfetto_version(con.state.work_perfetto)
        print(f'  {con.name}: perfetto version v{ver}')

        disabled_params = []
        for key, val in optional_params.items():
            if ver < val:
                disabled_params.append(key)

        lines = self.config_lines[:]
        for i, line in enumerate(lines):
            for param in disabled_params:
                if param in line:
                    lines[i] = '#' + line
                    print(f'  {con.name}: {param} requires v{optional_params[param]} and is disabled')

        txt = '\n'.join(lines) + '\n'
        con.state.config_txt = txt.encode('utf-8')

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--dut', '-d',
                        help='DUT to ssh to')
    parser.add_argument('--local', '-l', action='store_true',
                        help='DUT is local')
    parser.add_argument('--guest', '-g',
                        help='guest to ssh to')
    parser.add_argument('--vsh-guest', '-G',
                        help='guest to vsh to. Format is $DUT:vm_name. $DUT is optional if -d is also specified. $DUT may contain a port e.g. localhost:1234:termina')
    parser.add_argument('--android', '-a', metavar='SERIAL', nargs='?', const='',
                        help='guest to adb to')
    parser.add_argument('--config', '-c', default='configs/perfetto.cfg',
                        help='path to perfetto config')
    parser.add_argument('--time', '-t', type=int,
                        help='trace duration in seconds')
    parser.add_argument('--pps-producer', action='store_true',
                        help='start pps-producer if available')
    parser.add_argument('--output-dir', '-o', default='.',
                        help='path to output directory')
    parser.add_argument('--bin-dir',
                        help='where to find cros-trace-tool')
    parser.add_argument('--data-dir',
                        help='where to find configs and cross-compiled cros-trace-tool binaries')
    parser.add_argument('--verbose', '-v', action='store_true',
                        help='be verbose')

    args = parser.parse_args()

    if (args.dut is None and
        not args.local and
        args.guest is None and
        args.android is None and
        args.vsh_guest is None):
        print('error: no dut nor guest specified', file=sys.stderr)
        parser.print_help()
        sys.exit(2)

    if args.dut is not None and args.local:
        print(f'error: two duts ({args.dut} and local) are specified',
              file=sys.stderr)
        sys.exit(2)

    if len([a for a in [args.guest, args.android, args.vsh_guest] if a is not None]) >= 2:
        print(f'error: two or more guest targets are specified. Provided ({args.guest}, {args.android}, {args.vsh_guest})',
              file=sys.stderr)
        sys.exit(2)

    if args.vsh_guest and not ':' in args.vsh_guest and not args.dut:
        print(f'error: must provide host[:port]:vm_name as target, or also specify --dut to only provide vm_name. Provided ({args.vsh_guest} and {args.dut})',
              file=sys.stderr)
        sys.exit(2)

    global VERBOSE
    VERBOSE = args.verbose

    return args

def main():
    args = parse_args()
    session = Session(args)

    session.setup()
    session.start_tracing()
    session.wait_tracing()
    session.collect_traces()
    session.cleanup()

    session.merge_traces()

if __name__ == '__main__':
    main()
